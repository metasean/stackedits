# [V School - Module 0](https://scrimba.com/playlist/pG66Msa) feedback

## [V School Learning Philosophy](https://scrimba.com/c/ca3argsM)

### Base on research instead of just opinion
It could be more influential, if these concepts weren't presented as just personal opinion, but rather that they are ways to improve learning that have been demonstrated via research.  

### How to make practice beneficial
When talking about practice, simply practicing doesn't necessarily improve performance (in fact, improper practice can actually have detrimental effects).  

Here's a super simple list of ways to more deliberately practice - https://expertenough.com/2327/deliberate-practice-steps (However, I disagree strongly with, "The goal of deliberate practice is not to be enjoyed, the **_only_** goal is to improve your performance." [emphasis added]  From a motivation aspect, if a learner doesn't enjoy practice —at least to some degree— there's a problem.) 

### "Finding Answers"
As general guidance goes I completely agree.  

That said, it's critical to also teach learners "how" to search for answers and evaluate potential answers.  Simple things like deliberately checking the date of a blog post or SO answer, looking for version numbers, actually reading the question and response instead of just pasting the response, knowing to defer to maintained resources like MDN over older and less up-to-date resources like w3schools, etc. 

In fact, I actually encouraged my students to use DuckDuckGo as their default browser, because they could easily  `!js` (for JS documentation), `!mdn` (for HTML & CSS documentation), and `!so` (for StackOverflow).  (There's even a `!g` when the Duck isn't finding what you need.)

## Join the Module 0 Slack Workspace!
👍

Simplicity at it's best.

I didn't sign up for an account, but in most Slack groups there are also channels.  If y'all are using channels, then it could be useful to explain channels in general, as well as any conventions that y'all have.


## [How to Use Scrimba](https://scrimba.com/p/pG66Msa/cQ942nhN)

Overall a great introduction!

I'd be tempted to add thi to the notes.md file:
```
# Scrimba notes

## Keyboard shortcuts
Many programmers will use keyboard shortcuts instead of using graphical menus.  For example, in Scrimba there is no graphical way to save an edit, instead you'll need to use a keyboard shortcut that is based on your operating system (OS).  

To identify the keyboard shortcut to save/run in Scrimba in your OS, click the vertical ellipse in the upper-right corner of the screen.
<ig alt="highlighted screenshot of the vertical ellipse ⋮" src="https://gitlab.com/metasean/stackedits/raw/master/static/v-school__module-0__feedback/scrimba-shortcuts-expand-collapse.png" width="250px">

## Accessing Scrimba's Notes and Q&A Features
As mentioned in the Scrimba video, in the upper-right corner of the Scrimba interface you can click the left bracket "<" gliph to access  notes and Q&A functionality, as well as learning more about the specific video and accessing the related playlist.

<figure class="video_container">
  <video controls="true" allowfullscreen="true"   alt="screen capture highlighting the different features accessible via the '<' glyph" poster="https://gitlab.com/metasean/stackedits/raw/master/static/v-school__module-0__feedback/scrimba-shortcuts-expand-collapse.png" width="250px">
    <source src="https://gitlab.com/metasean/stackedits/raw/master/static/v-school__module-0__feedback/additional-scrimba-features.mp4" type="video/mp4">
  </video>
</figure>


## Markdown
- This file is a "markdown" file.
- It is identifiable as a "markdown" file because of the `.md` file extension.
- Markdown is a common type of text file among developers that uses simple inline characters to markup the text.  For example wrapping a word in pairs of asterisks will make it **bold**.
- [Mastering Markdown](https://guides.github.com/features/mastering-markdown/) is a great reference that provides some of the more common ways to accomplish things in markdown.
- [Markdown Tutorial](https://www.markdowntutorial.com/) is a great way to learn markdown.
```

---
---
---


## [External Link - Computer Setup](https://scrimba.com/c/cwyr4df9)

- I love your explanation of why you have just the markdown file in Scrimba, with a link to the actual video.
- I **_STRONGLY_** encourage you to encourage your students to use multiple browsers, and to even occasionally do a print preview.  Recruiters will frequently look at sites in non-Chrome browsers and have even been known to print out pages from applicants' websites (I wish I were joking about the printing thing, but I'm not). 


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTU5NzM2MjkzMCwxMTQ4MTQ0NjYzLDU2MD
E0MTM2NywtMjgzNjExNjgzLC0xOTUzMDE3ODk5LC0xNjk5NjAz
OTc4LDk2ODA0ODAyOSwtNzM5Njk3NTM1LDEwMTE3NDY2NDcsLT
E2MzUzMDIxMDcsLTQxNDUyNTAsNjc3MDM3NTAwLC0xOTQwMTU5
NTk5LC0yMTEyNjgyMjM3LC0xNTk0MjgwNzEyLDE0Nzc2NTczNz
YsLTE4OTgyODM0OTUsMTg1MzAyOTQyMywxNTk3ODQ1MzYxXX0=

-->