
**Jira Ticket:** #[](https://mygn.atlassian.net/browse/)

**Description and explanation of changes**  

- 

**Where should reviewer start?**

-

---

**Image showing how you feel about this PR**

<!-- Show how you feel about this PR with a giphy
     1) Search for the appropriate http://giphy.com 
     2) Click on your desired giphy
     3) Click the "Copy Link" text to the right of your desired giphy 
     4) Replace the src below with your copied link
     5) Replace the alt below with an appropriate textual description
-->

![ALT-TEXT-HERE](GIPHY-LINK-HERE?fixed_height&fixed_width)

---

<!-- For each of the following items, indicate the item has been completed (via `[x]`) or is not applicable (via `[n/a]`), and include any relevant notes -->

* [ ] **Tests**
	- includes corresponding unit tests (logic, data mutation, gui visibility, etc)
	- tests run locally
* [ ] **Localization (l10n) / Internationalization (i18n)**
	- all visible or screen reader text has been localized
* [ ] **Accessibility (a11y)**
	- GUI modification/edits/additions are accessible
	- axe "All issues" resolved
	  <!-- axe for Firefox - https://addons.mozilla.org/en-US/firefox/addon/axe-devtools/?src=search
	         axe for Chrome - https://chrome.google.com/webstore/detail/axe-web-accessibility-tes/lhdoppojpmngadmnindnejefpokejbdd -->
* [ ] **Documentation**
	- **JSDocs** updated to include descriptions, args, & return values for all new functions/methods
	- corresponding **README.md** documentation has been updated
	- corresponding **Confluence** documentation has been updated

---

<!--stackedit_data:
eyJoaXN0b3J5IjpbOTk1ODAyMDY0XX0=
-->